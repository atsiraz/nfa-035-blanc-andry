package fr.cnam.foad.nfa035.exam;

import org.springframework.context.annotation.ComponentScan;

/**
 * Commentez-moi
 */

@ComponentScan("fr.cnam.foad.nfa035.exam")
public class Nfa035ExamConfiguration {

}
